import logging
import sqlite3
import sys
import csv
import os
import atexit

"""Cette fonction sert à établir la connection avec la base de données"""
def db_connection(sql_file):
    connection = sqlite3.connect(sql_file)
    return connection

"""Cette fonction sert à fermer la connection avec la base de donnée"""
def db_close_connection(connection):
    if connection:
        connection.close()

"""Cette fonction sert à récuperer ce qui est contenu dans le fichier csv obtenu à 
la fin du mini-projet2"""
def get_csv(csv_file, delimiter):
    with open(csv_file, 'r') as csvfile: 
        reader = csv.reader(csvfile, delimiter=delimiter)
        data = []
        for row in reader:
            data.append(row)
    return(data)
    
"""Cette fonction vérifie si la base de données existe et sinon elle s'occupe de 
la créer"""
def db_verification_creation(sql_file):
    if(os.path.exists(sql_file)):
        logging.info('Data base already exists')
    else: 
        logging.debug("Creation of the data base because it doesn't exist")
    connection = db_connection(sql_file)
    connection.commit()
    db_close_connection(connection)

"""Cette fonction vérifie si la table SIV existe sinon elle la crée"""
def table_verification_creation(sql_file):
    connection = db_connection(sql_file)
    cursor = connection.cursor()
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
    if(cursor.fetchall() != []):
        logging.info('SIV table already exists')
    else :
        logging.debug("Creation of the SIV table because it doesn't exist")
        instruction = open("table_creation.txt", 'r')
        cursor.execute(instruction.read())
        logging.info('The table is created')
    connection.commit()
    cursor.close()
    db_close_connection(connection)

"""Cette fonction vérifie si les données sont déjà contenues dans la table, 
si c'est le cas elle les mets à jours sinon elle les insère"""
def insert_update_table(sql_file, data):
    del data[0]
    connection = db_connection(sql_file)
    cursor = connection.cursor()
    insertion = 0
    update = 0
    id = 0
    for row in data:
        id += 1
        ligne = str(id)
        data_immatriculation = row[3]
        cursor.execute("SELECT * FROM SIV WHERE immatriculation=?",(data_immatriculation,))
        if(cursor.fetchall() == []):
            cursor.execute("INSERT OR IGNORE INTO SIV VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", (ligne, row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12], row[13], row[14], row[15], row[16], row[17], row[18]))
            insertion += 1
        else:
            cursor.execute('''UPDATE SIV SET adresse_titulaire= ?, nom= ?,prenom= ?, date_immatriculation= ?, vin= ?, marque= ?, denomination_commerciale= ?, couleur= ?, carroserie= ?, categorie= ?, cylindree= ?, energie= ?, places= ?, poids= ?, puissance= ?, type= ?, variante= ?, version= ? WHERE immatriculation = ?
            ''', (row[0], row[1], row[2], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12], row[13], row[14], row[15], row[16], row[17], row[18], data_immatriculation))  
            update += 1
    logging.info('%d data have been inserted and/or %d data have been updated', insertion, update)
    connection.commit()
    cursor.close()
    db_close_connection(connection)

"""Cette fonction est la fonction principale du programme qui prend en entrée 
le fichier csv et le fichier sql et qui permet d'exécuter les fonctions vues ci-dessus"""
def main(sql_file, csv_file):
    logging.basicConfig(filename='logging_file.log', level=logging.DEBUG, format='%(asctime)s: %(message)s')
    logging.info('Start of the program')
    data = get_csv(csv_file, ';')
    db_verification_creation(sql_file)
    table_verification_creation(sql_file)
    insert_update_table(sql_file, data)
    logging.info('End of the program')

if __name__ == '__main__':
    main("database_file.sqlite3", "cars_csv_file.csv")